# FuzzyModule #

A C# fuzzy logic module based on the C++ code from "Programming Game AI by Example" by Mat Buckland.

I created this code for a school assignment. For this assignment we had to create a game with intelligent entities which made their decisions based on fuzzy logic. The module can be used in other projects for decision making as well.