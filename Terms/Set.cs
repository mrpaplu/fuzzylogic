﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameAI.Components.Fuzzy.Terms
{
    public class Set : FuzzyTerm
    {
        #region Fields and properties

        private FuzzySet FuzzySet { get; set; }

        #endregion

        #region Constructor

        public Set(FuzzySet set)
        {
            FuzzySet = set;
        }

        #endregion

        #region FuzzyTerm

        public override double GetDOM()
        {
            return FuzzySet.Dom;
        }

        public override void ClearDOM()
        {
            FuzzySet.ClearDOM();
        }

        public override void ORwidthDOM(double value)
        {
            FuzzySet.ORwithDOM(value);
        }

        #endregion
    }
}
