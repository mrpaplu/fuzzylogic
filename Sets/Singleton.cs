﻿namespace GameAI.Components.Fuzzy.Sets
{
    public class Singleton : FuzzySet
    {
        #region Fields and properties

        /// <summary>
        /// The peak of this FLV
        /// </summary>
        private double _peak;

        /// <summary>
        /// The left offset of this FLV
        /// </summary>
        private double _left;

        /// <summary>
        /// The right offset of this FLV
        /// </summary>
        private double _right;

        #endregion

        #region Constructor

        public Singleton(double left, double peak, double right)
            : base(peak)
        {
            _peak = peak;
            _left = left;
            _right = right;
        }

        #endregion

        #region Set

        /// <summary>
        /// Calculates the degree of membership for a certain value
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public override double CalculateDom(double val)
        {
            if (val >= _peak-_left
                && val <= _peak-_right)
                return 1.0;

            // Out of range, return zero
            return 0.0;
        }

        #endregion
    }
}
