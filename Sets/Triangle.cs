﻿namespace GameAI.Components.Fuzzy.Sets
{
    public class Triangle : FuzzySet
    {
        #region Fields and properties

        /// <summary>
        /// The peak value of the FLV
        /// </summary>
        private double _peak;

        /// <summary>
        /// The left offset of the FLV
        /// </summary>
        private double _left;

        /// <summary>
        /// The right offset of the FLV
        /// </summary>
        private double _right;

        #endregion

        #region Constructor

        public Triangle(double left, double peak, double right)
            : base(peak)
        {
            _peak = peak;
            _left = left;
            _right = right;
        }

        #endregion

        #region Set

        /// <summary>
        /// Calculates the degree of membership for a value
        /// </summary>
        /// <returns></returns>
        public override double CalculateDom(double val)
        {
            // Prevent divide by zero
            if ((_right == 0.0 && _peak == val) ||
                (_left == 0.0 && _peak == val))
            return 1.0;

            // Find DOM if left of center
            if (val <= _peak && val >= _peak - _left)
            {
                double grad = 1.0 / _left;
                return grad * (val - (_peak - _left));
            }

            // Find DOM if right of center
            if (val > _peak && val < (_peak + _right))
            {
                double grad = 1.0 / _right;
                return grad * (val - _peak) + 1.0;
            }

            // Out of range, return zero
            return 0.0;
        }

        #endregion
    }
}
