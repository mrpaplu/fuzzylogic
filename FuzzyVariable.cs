﻿using System;
using System.Collections.Generic;
using GameAI.Components.Fuzzy.Sets;
using GameAI.Components.Fuzzy.Terms;

namespace GameAI.Components.Fuzzy
{
    public class FuzzyVariable
    {
        #region Properties and fields

        /// <summary>
        /// A map of the fuzzy sets that comprise this variable
        /// </summary>
        private Dictionary<string, FuzzySet> Sets { get; set; }

        /// <summary>
        /// Minimum value of the range of this variable
        /// </summary>
        private double _minRange;

        /// <summary>
        /// Maximum value of the range of this variable
        /// </summary>
        private double _maxRange;

        #endregion

        #region Constructor

        public FuzzyVariable()
        {
            Sets = new Dictionary<string, FuzzySet>();
        }

        #endregion

        #region Adding sets

        /// <summary>
        /// This method is called with the upper and lower bound of a set each time a 
        /// new set is added to adjust the upper and lower range values accordingly
        /// </summary>
        private void AdjustRangeToFit(double min, double max)
        {
            if (min < _minRange)
                _minRange = min;
            if (max > _maxRange)
                _maxRange = max;
        }

        /// <summary>
        /// Adds a triangular set to the variable
        /// </summary>
        public Set AddTriangular(string name, double min, double peak, double max)
        {
            FuzzySet fzSet = new Triangle(peak - min, peak, max - peak);
            Set set = new Set(fzSet);
            Sets.Add(name, fzSet);
            AdjustRangeToFit(min, max);
            return set;
        }

        /// <summary>
        /// Adds a max shoulder set to the variable
        /// </summary>
        public Set AddRightShoulder(string name, double min, double peak, double max)
        {
            FuzzySet fzSet = new RightShoulder(peak - min, peak, max - peak);
            Set set = new Set(fzSet);
            Sets.Add(name, fzSet);
            AdjustRangeToFit(min, max);
            return set;
        }

        /// <summary>
        /// Adds a min shoulder set to the variable
        /// </summary>
        public Set AddLeftShoulder(string name, double min, double peak, double max)
        {
            FuzzySet fzSet = new LeftShoulder(peak - min, peak, max - peak);
            Set set = new Set(fzSet);
            Sets.Add(name, fzSet);
            AdjustRangeToFit(min, max);
            return set;
        }

        /// <summary>
        /// Adds a singleton set to the variable
        /// </summary>
        public Set AddSingleton(string name, double min, double peak, double max)
        {
            FuzzySet fzSet = new Singleton(peak - min, peak, max - peak);
            Set set = new Set(fzSet);
            Sets.Add(name, fzSet);
            AdjustRangeToFit(min, max);
            return set;
        }

        #endregion

        #region Fuzzification and defuzzification

        /// <summary>
        /// Takes a crisp value and calculates its degree of membership for each set
        //  in the variable.
        /// </summary>
        /// <param name="value"></param>
        public void Fuzzify(double value)
        {
            if (value < _minRange || value > _maxRange) return;

            foreach (FuzzySet set in Sets.Values)
            {
                set.Dom = set.CalculateDom(value);
            }
        }

        /// <summary>
        /// Defuzzifies the value by averaging the maxima of the sets that have fired
        //
        // OUTPUT = sum (maxima * DOM) / sum (DOMs) 
        /// </summary>
        public double DefuzzifyMaxAV()
        {
            double bottom = 0.0;
            double top = 0.0;

            foreach (FuzzySet set in Sets.Values)
            {
                bottom += set.Dom;

                top += set.RepresentativeValue * set.Dom;
            }

            if (bottom == 0.0) 
                return 0.0;

            return top / bottom;
        }

        /// <summary>
        /// Defuzzify the variable using the centroid method
        /// </summary>
        /// <param name="numberOfSamples"></param>
        /// <returns></returns>
        public double DefuzzifyCentroid(int numberOfSamples)
        {
            double stepSize = (_maxRange - _minRange) / numberOfSamples;

            double totalArea = 0.0;
            double sumOfMoments = 0.0;

            //step through the range of this variable in increments equal to StepSize
            //adding up the contribution (lower of CalculateDOM or the actual DOM of this
            //variable's fuzzified value) for each subset. This gives an approximation of
            //the total area of the fuzzy manifold.(This is similar to how the area under
            //a curve is calculated using calculus... the heights of lots of 'slices' are
            //summed to give the total area.)
            //
            //in addition the moment of each slice is calculated and summed. Dividing
            //the total area by the sum of the moments gives the centroid. (Just like
            //calculating the center of mass of an object)
            for (int i = 1; i <= numberOfSamples; ++i)
            {
                //for each set get the contribution to the area. This is the lower of the 
                //value returned from CalculateDOM or the actual DOM of the fuzzified 
                //value itself   
                foreach (FuzzySet set in Sets.Values)
                {
                    double contribution = Math.Min(
                        set.CalculateDom(_minRange + i * stepSize),
                        set.Dom
                    );

                    totalArea += contribution;

                    sumOfMoments += (_minRange + i * stepSize) * contribution;
                }
            }

            if (totalArea == 0.0)
                return 0.0;

            return sumOfMoments / totalArea;
        }

        #endregion
    }
}
